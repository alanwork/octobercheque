<?php namespace Printcheque\Cheque\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreatePrintchequeCheque extends Migration
{
    public function up()
    {
        Schema::create('printcheque_cheque_', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->date('chequedate')->nullable();
            $table->string('payee', 250)->nullable();
            $table->integer('bank')->nullable();
            $table->double('amount', 10, 0)->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('printcheque_cheque_');
    }
}
