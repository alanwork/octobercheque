<?php namespace Printcheque\Cheque\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdatePrintchequeCheque extends Migration
{
    public function up()
    {
        Schema::table('printcheque_cheque_', function($table)
        {
            $table->renameColumn('bank', 'bank_id');
        });
    }
    
    public function down()
    {
        Schema::table('printcheque_cheque_', function($table)
        {
            $table->renameColumn('bank_id', 'bank');
        });
    }
}
