<?php namespace Printcheque\Cheque\Models;

use Model;

/**
 * Model
 */
class Cheque extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;


    /**
     * @var string The database table used by the model.
     */
    public $table = 'printcheque_cheque_';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $belongsTo =[
        'bank' =>[
            'PrintCheque\Bank\Models\Bank',
            'table' => 'printcheque_bank_',
            'order' => 'name'
        ]
    ];
}
